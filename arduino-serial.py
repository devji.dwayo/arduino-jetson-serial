import serial
import time

# create object of serial
arduino = serial.Serial(port='/dev/ttyUSB0', baudrate=115200, timeout=.1)


def write_read(x):
    # write to arduino
    arduino.write(bytes(x, 'utf-8'))

    # sleep for 0.05s
    time.sleep(0.05)

    try:
        # read from arduino
        data = int(arduino.readline())
    except ValueError:
        data = -1
    return data


while True:
    # take input from user
    num = input("Enter a number: ")

    # read and assign
    value = write_read(num)

    # printing the value
    print(value)