int x;

void setup() {
    // setting-up baudrate
    Serial.begin(115200);
    Serial.setTimeout(1);
}

void loop() {
    // block 'until' something is available
    while (!Serial.available());
    
    // read buffer and convert to int
    x = Serial.readString().toInt();
    
    // add one and send back    
    Serial.print(x + 1);
}
